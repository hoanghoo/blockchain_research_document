# Giá và biến động giá của ERC20 - BEP20

- ERC20, BEP20 là các token chạy trên Ethereum, BSC mà không thể chạy độc lập được nên giá của chúng thường được gắn với ETH/BNB
- Giá khởi tạo của 1 token  được định nghĩa bởi người tạo ra bể thanh khoản (liquid pool) liên quan tới đồng token đó, với khả năng thanh khoản trong bể đó.
- Biến động giá phụ thuộc vào thị trường
- Tài liệu này đề cập tới vấn đề định giá token mới khởi tạo và các kịch bản biến động giá

### Các khái niệm được nhắc tới
- Liquid Pool : bể thanh khoản
- Liquidity Provider: người cung cấp thanh khoản
- Uniswap: là một tập hợp contract trên Ethereum và cho phép hoán đổi token một cách phi tập trung lớn nhất trên thế giới hiện nay
- Stablecoin: loại token được thế chấp bởi tài sản thực
- Non-stablecoin: loại token không được thế chấp bởi tài sản thực

## 1. Bể thanh khoản
- Khác với giao thức sổ lệnh truyền thống, khi phải khớp lệnh thì mới có thể thực hiện giao dịch được, bể thanh khoản là 1 loại contract giúp chuyển đổi giữa các loại token một cách tức thì
- Ở giao thức sổ lệnh, giá khớp lệnh chính là giá token. Nhưng ở bể thanh khoản, giá token tùy thuộc vào các loại bể thanh khoản, ở tài liệu này sẽ áp dụng bể thanh khoản lớn nhất hiện tại là Uniswap
- Với bể thanh khoản, người dùng có thể kiếm thu nhập nhờ việc gửi các cặp token vào bể và hưởng phí giao dịch. 

## 2. Uniswap
#### 2.1. Giới thiệu
- Uniswap là một giao thức trao đổi phi tập trung trên Ethereum. Nói chính xác hơn, nó là một giao thức thanh khoản tự động. Không có bất cứ cuốn sổ lệnh hay một đơn vị trung gian để các giao dịch có thể được thực hiện. Uniswap cho phép người dùng mua bán không qua trung gian với mức độ phi tập trung và không cần sự kiểm duyệt.
#### 2.2. Khởi tạo giá
- Các nhà cung cấp thanh khoản tạo ra một thị trường bằng cách ký gửi giá trị tương đương hai mã token. Chúng có thể là một token ETH và một mã token ERC-20 hoặc hai mã token ERC-20. Các nhóm này thường được tạo thành từ một stablecoinnhư DAI, USDC, hoặc USDT, tuy nhiên điều này không phải là một yêu cầu bắt buộc. Đổi lại, các nhà cung cấp thanh khoản sẽ nhận được “token thanh khoản”, thể hiện bằng thị phần của họ trong toàn bộ bể thanh khoản. Các token thanh khoản này có thể được đổi thưởng theo phần mà chúng đại diện trong bể.
#### 2.3. Cách thức hoạt động
- Xem xét bể thanh khoản ETH/LONG. Gọi phần ETH của bể là x và phần LONG là y. Uniswap sẽ lấy hai đại lượng này và nhân chúng với nhau để tính tổng thanh khoản của bể. Tích này được gọi là k. Đây được gọi là Constant Product Market Maker Model - Hằng Số Tạo Lập Thị trường. 
- Trong các pool của Uniswap luôn tồn tại một công thức là:  `x * y = k`
	-   Với  `x`  là số lượng token A.
	-   Và  `y`  là lượng token B

- Người tạo pool hay hay chính là LP đầu tiên của pool sẽ quyết định hệ số này bằng việc deposit với lượng token tùy ý. Và điều này sẽ quyết định giá của token.
- Giả sử Alice mua 1 ETH bằng cách sử dụng bể thanh khoản ETH/LONG. Bằng cách này, Alice đã tăng được phần LONG của bể và giảm phần ETH của bể. Điều này đồng nghĩa với việc giá ETH tăng vì tổng thanh khoản (k) của bể lại luôn là một hằng số. Cơ chế này cũng chính là thứ quyết định ra giá Cuối cùng. Giá phải trả cho ETH này được dựa trên mức độ dịch chuyển tỷ lệ giữa x và y của một giao dịch nhất định.
- Các cặp token này có giá trị tương đương nhau (lưu ý là giá trị chứ không phải số lượng)
- Tham khảo [[Uniswap]](https://app.uniswap.org/#/swap)

## 3. Ví dụ
- **Khởi tạo giá 1 LONG = 0.001 ETH**
	- LP tạo pool và gửi cặp LONG-ETH tương ứng vào pool. Giả sử gửi 1000 LONG và 1ETH vào bể.
	- Ta có `x * y = k ⇔ 1000 * 1 = 1,000` Giá 1 ETH=1000 LONG và 1 LONG = 0.001ETH

- **Swap LONG lấy ETH**
	Trader A vào pool này và swap 100 LONG+ 0.3% phí để đổi lấy ETH.

		=> y’ = 100 + 1000 = 1100 LONG.

	**k**  không đổi, vẫn bằng 1,000.

		=> x' = 0.9091 ETH.

	Suy ra trader nhận được x - x' = 1 - 0.9091 = 0.0909 ETH, tương đương giá trị 100 LONG.

		=> giá 1 LONG = 0.000901 ETH, giảm 9,9% so với giá ban đầu.

- **Swap ETH lấy LONG**
	Trader A vào pool này và swap 0.1 ETH+ 0.3% phí để đổi lấy LONG.

		=> x’ = 1 + 0.1 = 1.1 ETH.

	**k**  không đổi, vẫn bằng 1,000.

		=> y' = 909.091 LONG.

	Suy ra trader nhận được y - y' = 1000 - 909.091 = 90.909 LONG, tương đương giá trị 0.1ETH

		=> giá 1 LONG = 0.0011 ETH, tăng 10% so với giá ban đầu.
		
## 4. Biến động giá
Qua các ví dụ ở trên, ta có thể thấy giá của 1 đồng token phụ thuộc vào tỉ lệ token trong bể thanh khoản, mà bể thanh khoản là phản ánh của thị trường. Nói cách khác, giá của 1 đồng token phụ thuộc vào thị trường như các đồng tiền truyền thống khác
Tới đây ta phân tích các yếu tố tác động đến giá của 2 loại token, stablecoin và non-stablecoin
#### 4.1. Stablecoin
- Giả sử 1 token LONG được một cơ quan đảm bảo rằng nó tương ứng với 1 VND, chỉ cần chuyển 1 LONG cho cơ quan đó thì sẽ nhận lại 1 VND
- Bể thanh khoản LONG với 1 đồng tiền nào đó sẽ được thiết lập, ở stable coin thường là ETH/BNB
	- Nếu lượng mua vào LONG tăng cao, (swap ETH lấy LONG) giá trị của token LONG cao hơn giá trị thực (> 1VND), lúc này người dùng sẽ có xu hướng đổi LONG lấy ETH vì lúc này  mua ETH bằng LONG sẽ rẻ hơn mua bằng VND. Điều này lại làm giá LONG giảm xuống
	- Nếu lượng bán ra LONG tăng cao (swap LONG lấy ETH), giá trị của token LONG thấp hơn giá trị thực (> 1VND), lúc này người dùng sẽ có xu hướng đổi ETH lấy LONG vì lúc này sẽ mua được LONG với giá rẻ và bán lại cho cơ quan đảm bảo để ăn chênh lệch. Điều này lại làm giá LONG tăng lên
	- Chính 2 điều này làm giá của 1 stablecoin luôn giao động với biên độ nhỏ quanh giá trị thực của nó
#### 4.2. Non-Stablecoin
- Giả sử 1 token LONG được phát hành với chỉ định 1000LONG = 1ETH,
- Bể thanh khoản LONG với 1 đồng tiền nào đó sẽ được thiết lập, ở non-stable coin thường là ETH/BNB hoặc 1 stablecoin
	- Nếu lượng mua vào LONG tăng cao, (swap ETH lấy LONG) giá trị của token LONG tăng cao. 
	- Nếu lượng bán ra LONG tăng cao (swap LONG lấy ETH), giá trị của token LONG giảm
	- Biến động của LONG giống như quy luật của thị trường truyền thống

