﻿# Tài Liệu Kĩ Thuật - ERC20

## Tổng quan

- **ERC20 (Ethereum Request for Comment)** là tên gọi của một bộ các tiêu chuẩn mà những token được phát triển trên nền tảng blockchain của Ethereum phải tuân thủ theo. Để tạo ra các token trên nền tảng của Ethereum thì phải lập ra các hợp đồng thông minh (smart contract), và các smart contract này phải được lập trình theo tiêu chuẩn ERC20
- Cung cấp các chức năng cơ bản: transfer, cho phép token được sử dụng lại bởi các ứng dụng khác: ví, sàn giao dịch phi tập trung

## Các quy tắc
- Quy tắc tùy chọn
	- **Token Name** : Tên của token 
	- **Symbol** : kí hiệu của token
	- **Decimals** : Số thập phân (lên đến 18), quy định về số lượng chữ số thập phân trong đơn vị của token. Decimals của VDS là 18, nghĩa là đơn vị nhỏ nhất của VDS là 0.000000000000000001 VDS. 
- Quy tắc bắt buộc
	- **TotalSupply** : tổng nguồn cung của token
	- **BalanceOf** : Số dư của một ví đang có
	- **Transfer** : chuyển token từ ví của bạn sang ví của người dùng khác bằng cách cung cấp địa chỉ ví của người nhận và số token muốn chuyển.
	- **TransferFrom** : chuyển từ tài khoản này sang tài khoản khác, quy tắc này cũng khá tương tự như transfer nhưng tiện dụng hơn, để làm được điều này thì cần được ủy quyền.
	- **Approve** : Ủy quyền cho người khác sử dụng một số lượng token của mình
	- **Allowance** : kiểm tra số dư của token đã ủy quyền. 

## Các phương thức

| Method 																| Return                  |
| ------------- 															| ------------------------------ |
| `name()`      															| Name of token       |
| `decimals()` 															| Symbol of token     |
| `symbol()`      														| Name of token       |
| `totalSuppy()`  													| Token total supply     |
| `balanceOf(_owner)`      										| The account balance of _owner       |
| `transfer(_recipient, _amout)`  							| Transfer to _recipient _amount token     |
| `approve(_spender, _amount)`  							| Approve _spender can use my  _amount token     |
| `allowance(_owner, _spender)`      					| Check token approve left of _owner approved _spender       |
| `transferFrom(_sender ,_recipient, _amout)`  	| Transfer from _sender to _recipient _amount token (need to approve)     |


## Phí giao dịch

### Định nghĩa
-	Phí giao dịch trên Ethereum (phí Gas)  đơn thuần là các khoản thanh toán được thực hiện bởi người dùng để đổi lấy năng lượng tính toán cần thiết nhằm xử lí và xác thực các giao dịch trên chuỗi khối Ethereum cùng một số yếu tố khác không phổ biến như khấu trừ chi phí cho người theo dõi, người tạo hợp đồng và cũng có thể thanh toán cho lượng sử dụng tăng lên của bộ nhớ
-	Phí giao dịch được thanh toán bằng native token của mạng mà cụ thể ở đây là đồng ETH

### Cách tính
- Phí giao dịch ước tính: `TxFee = Gas_Price * Gas_Limit)`
- Phí giao dịch thực tế: `TxFee = Gas_Price * Gas_Used)`
> Trong đó:
> - Gas Price: chi phí một Gas  (đơn vị thường dùng là Gwei (1 Gwei = 10-9 ETH))
> - Gas Limit: số gas tối đa có thể trả cho giao dịch. Trong trường hợp Gas_Limit không đủ để thực hiện giao dịch, giao dịch sẽ rơi vào trạng thái hết Gas và thất bại, số Gas đã được sử dụng để đổi lấy năng lượng tính toán sẽ không hoàn lại. Số gas tối thiểu là 21000
> - Gas Used: số gas đã được sử dụng thực tế. Số gas không được dùng sẽ được trả lại

### Vai trò của Gas_Price
- 2 transaction tương tự nhau sẽ tốn một lượng gas như nhau. Điều này cũng có thể hiểu vì gas là chi phí cho năng lượng tính toán, 2 giao dịch có input và điều kiện giống nhau sẽ tiêu tốn một năng lượng tính toán như nhau
- Những người xác thực trong mạng, vì lợi ích kinh tế nên họ sẽ chọn các giao dịch có phí gas cao. Vì vậy  Gas_Price càng cao thì giao dịch được thực hiện càng nhanh
- Trên mạng Ethereum, do khối lượng giao dịch lớn nên Gas_Price thay đổi liên tục.
- [[GasTracker]](https://etherscan.io/gastracker)
- [[Ethereum Average Gas Price]](https://etherscan.io/chart/gasprice)

### Phí giao dịch thực tế (dựa trên smart contract đi kèm)
> Vì phí giao dịch phụ thuộc vào độ phức tạp, tài nguyên smart contract sử dụng nên không có công  thức tính toán hay báo cáo chính thức nào. Chi phí này được tính toán trên việc khảo sát thực tế các giao dịch mạng Ethereum mainnet -  testnet
- Phí deploy: 1.800.000 gas < ... < 1.900.000 gas
- Transfer Gas Limit: 21.000 gas < ... < 77.000 gas
- Average Gas Price: 19.36 Gwei
- => Transfer Fee Max: 0.00040656 ETH < ... < 0.00149072 ETH
- ==> Transfer Fee Max (10AM - 8/6):  1.01$ < ... < 3.70$

## Thời gian giao dịch
- Thời gian giao dịch phụ thuộc vào Gas_Price, thời gian này thay đổi liên tục. Bởi vậy trước khi thực hiện giao dịch hãy tham khảo thông số này
- Trong điều kiện lý tưởng, một giao dịch trên Ethereum mất khoảng 13s
- [[GasTracker]](https://etherscan.io/gastracker)

## Tạo token theo chuẩn ERC20

- Step 1: Viết Smart Contract
	> Smart Contract trên Ethereum được viết bằng ngôn ngữ Solidity
	
- Step 2: Complie
	> Complie sẽ sinh ra 2 thành phần: ABI và Bytecode
	
	![alt](https://i1.wp.com/i.imgur.com/vYcXagJ.png?resize=100%2C241&ssl=1 =400x300)

	- ABI (Application Binary Interface) dùng để xác định hợp đồng thông minh được gọi và trả về theo định dạng mong đợi. Phần này thường được nhúng vào ứng dụng có sử dụng smart contract như một interface của smart contract
	- Bytecode: đoạn mã có thể được thực thi trên EVM tạo thành các smart contract

- Step 3: Deploy
	> Tạo một giao dịch deploy contract gửi tới địa chỉ 0 (0x0) của mạng

## Các chuẩn ERC khác

- **ERC223**
Tiêu chuẩn ERC223 được thiết kế để ngăn chặn việc chuyển token xảy ra sự cố, khắc phục hậu quả bị mất đến 3 triệu đô nói trên. Ngoài ra, ERC223 cũng giúp giảm chi phí giao dịch so với ERC20. Một số token ERC223 như Lendo (ELT), ProntaPay (PRO)…
- **ERC721**
Được tạo ra với ý tưởng đi ngược lại hoàn toàn với tính chất của các coin/token, là tính thay thế (fungible). Tính chất này có nghĩa là mỗi đồng coin/token đều có giá trị như nhau và được xử lý như nhau
- **ERC1155**
Là một sự kết hợp của cả ERC20 và ERC721, vì các token ERC1155 vừa là các token có thể thay thế (fungible), vừa không thể thay thế (non-fungible)
- **ERC621**
Được phát triển để bổ sung tính tăng có thể tăng hoặc giảm nguồn cung cho token
- **ERC777**
Là một phiên bản có cải tiến thêm về mặt bảo mật và nhiều tính năng nâng cao khác. ERC777 cung cấp nhiều tùy chọn khi xử lý các giao dịch liên quan đến token. Các token ERC777 sẽ tương thích với các token ERC20 theo hướng ngược lại.
#### Tham khảo: [ERC](https://eips.ethereum.org/erc)

