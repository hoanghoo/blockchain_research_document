﻿# Tài Liệu Kĩ Thuật - BEP20

## Tổng quan

- **BEP20** là tên gọi của một bộ các tiêu chuẩn mà những token được phát triển trên nền tảng blockchain của **Binance Smart Chain** phải tuân thủ theo.
- Tiêu chuẩn này dựa trên chuẩn ERC20 của Ethereum, nhằm tăng cường khả năng tương thích tối đa với EVM cũng như tận dụng hệ sinh thái rộng lớn của Ethereum
- Cung cấp các chức năng cơ bản: transfer, cho phép token được sử dụng lại bởi các ứng dụng khác: ví, sàn giao dịch phi tập trung và **di chuyển tài sản giữa Binance Chain và Binance Smart Chain**
- Nói một cách khác, BEP20 chính là ERC20 chạy trên Binance Smart Chain. Nghĩa là nếu BEP20 không liên kết cross-chain mà chỉ chạy độc lập trên Binance Smart Chain, nó tương tự ERC20

## Các quy tắc
- Quy tắc tùy chọn
	- **Token Name** : Tên của token 
	- **Symbol** : kí hiệu của token
	- **Decimals** : Số thập phân (lên đến 18), quy định về số lượng chữ số thập phân trong đơn vị của token. Decimals của VDS là 18, nghĩa là đơn vị nhỏ nhất của VDS là 0.000000000000000001 VDS. 
	- **Owner** : Chủ sở hữu hợp đồng thông minh, chính là người đã khởi tạo giao dịch triển khai hợp đồng thông minh lên mạng
- Quy tắc bắt buộc
	- **TotalSupply** : tổng nguồn cung của token
	- **BalanceOf** : Số dư của một ví đang có
	- **Transfer** : chuyển token từ ví của bạn sang ví của người dùng khác bằng cách cung cấp địa chỉ ví của người nhận và số token muốn chuyển.
	- **TransferFrom** : chuyển từ tài khoản này sang tài khoản khác, quy tắc này cũng khá tương tự như transfer nhưng tiện dụng hơn, để làm được điều này thì cần được ủy quyền.
	- **Approve** : Ủy quyền cho người khác sử dụng một số lượng token của mình
	- **Allowance** : kiểm tra số dư của token đã ủy quyền. 

## Các phương thức

| Method 																| Return                  |
| ------------- 															| ------------------------------ |
| `name()`      															| Name of token       |
| `decimals()` 															| Symbol of token     |
| `symbol()`      														| Name of token       |
| `getOwner()`      													| Returns the bep20 token owner which is necessary for binding with bep2 token       |
| `totalSuppy()`  													| Token total supply     |
| `balanceOf(_owner)`      										| The account balance of _owner       |
| `transfer(_recipient, _amout)`  							| Transfer to _recipient _amount token     |
| `approve(_spender, _amount)`  							| Approve _spender can use my  _amount token     |
| `allowance(_owner, _spender)`      					| Check token approve left of _owner approved _spender       |
| `transferFrom(_sender ,_recipient, _amout)`  	| Transfer from _sender to _recipient _amount token (need to approve)     |


## Phí giao dịch

### Định nghĩa
-	Phí giao dịch trên Binance Smart Chain (phí Gas)  đơn thuần là các khoản thanh toán được thực hiện bởi người dùng để đổi lấy năng lượng tính toán cần thiết nhằm xử lí và xác thực các giao dịch trên chuỗi khối  cùng một số yếu tố khác không phổ biến như khấu trừ chi phí cho người theo dõi, người tạo hợp đồng và cũng có thể thanh toán cho lượng sử dụng tăng lên của bộ nhớ
-	Phí giao dịch được thanh toán bằng native token của mạng mà cụ thể ở đây là đồng BNB
-	Do cùng kiểu xác thực như Ethereum (sử dụng EVM) nên cách tính phí giao dịch là như nhau

### Cách tính
- Phí giao dịch ước tính: `TxFee = Gas_Price * Gas_Limit)`
- Phí giao dịch thực tế: `TxFee = Gas_Price * Gas_Used)`
> Trong đó:
> - Gas Price: chi phí một Gas  (đơn vị thường dùng là Gwei (1 Gwei = 10-9 BNB))
> - Gas Limit: số gas tối đa có thể trả cho giao dịch. Trong trường hợp Gas_Limit không đủ để thực hiện giao dịch, giao dịch sẽ rơi vào trạng thái hết Gas và thất bại, số Gas đã được sử dụng để đổi lấy năng lượng tính toán sẽ không hoàn lại. Số gas tối thiểu là 21000
> - Gas Used: số gas đã được sử dụng thực tế. Số gas không được dùng sẽ được trả lại

### Vai trò của Gas_Price
- 2 transaction tương tự nhau sẽ tốn một lượng gas như nhau. Điều này cũng có thể hiểu vì gas là chi phí cho năng lượng tính toán, 2 giao dịch có input và điều kiện giống nhau sẽ tiêu tốn một năng lượng tính toán như nhau
- Những người xác thực trong mạng, vì lợi ích kinh tế nên họ sẽ chọn các giao dịch có phí gas cao. Vì vậy  Gas_Price càng cao thì giao dịch được thực hiện càng nhanh
- Trên mạng Binance Smart Chain, Gas Price nhìn chung giữ ở mức ổn định là ~7 Gwei
- [[GasTracker]](https://bscscan.com/chart/gasprice)

### Phí giao dịch thực tế (dựa trên smart contract đi kèm)
> Vì phí giao dịch phụ thuộc vào độ phức tạp, tài nguyên smart contract sử dụng nên không có công  thức tính toán hay báo cáo chính thức nào. Chi phí này được tính toán trên việc khảo sát thực tế các giao dịch mạng Binance Smart Chain mainnet -  testnet
- Phí deploy: 1.800.000 gas < ... < 1.900.000 gas
- Transfer Gas Limit: 21.000 gas < ... < 77.000 gas
- Average Gas Price: 5 Gwei
- => Transfer Fee Max: 0.000105 BNB< ... < 0.000385 BNB
- ==> Transfer Fee Max (10AM - 8/6):  0.04$ < ... < 0.13$

## Thời gian giao dịch
- Thời gian giao dịch phụ thuộc vào Gas_Price, thời gian này thay đổi liên tục. Gas Price của Binance Smart Chain giữ ở mức ổn định nên thời gian giao dịch cũng ở mức ổn định.
- Thời gian giao dịch trên Binance Smart Chain là 3s

## Tạo token theo chuẩn BEP20

- Step 1: Viết Smart Contract
	> Smart Contract trên Ethereum được viết bằng ngôn ngữ Solidity
	
- Step 2: Complie
	> Complie sẽ sinh ra 2 thành phần: ABI và Bytecode
	
	![alt](https://i1.wp.com/i.imgur.com/vYcXagJ.png?resize=100%2C241&ssl=1 =400x300)

	- ABI (Application Binary Interface) dùng để xác định hợp đồng thông minh được gọi và trả về theo định dạng mong đợi. Phần này thường được nhúng vào ứng dụng có sử dụng smart contract như một interface của smart contract
	- Bytecode: đoạn mã có thể được thực thi trên EVM tạo thành các smart contract

- Step 3: Deploy
	> Tạo một giao dịch deploy contract gửi tới địa chỉ 0 (0x0) của mạng

## Các chuẩn BEP khác

- **BEP-1**: Purpose and Guidelines of BEP
- **BEP-2**: Tokens on Binance Chain
- **BEP-3**: HTLC and Atomic Peg
- **BEP-6**: Delist Trading Pairs on Binance Chain
- **BEP-8**: Mini-BEP2 Tokens
- **BEP-9**: Time Locking of Tokens on Binance Chain
- **BEP-10**: Registered Types for Transaction Source
- **BEP-12**: Introduce Customized Scripts and Transfer Memo Validation
- **BEP-18**: State sync enhancement
- **BEP-19**: Introduce Maker and Taker for Match Engine
- **BEP-20**: Tokens on Binance Smart Chain
- **BEP-70**: List and Trade BUSD Pairs
- **BEP-67**: Price-based Order
- **BEP-82**: Token Ownership Changes
- **BEP-84**: Mirror BEP20 to Binance Chain
- **BEP-86**: Dynamic Extra Incentive For BSC Relayers
- **BEP-87**: Token Symbol Minimum Length Change
- **BEP-89**: Visual Fork of Binance Smart Chain
- **BEP-91**: Increase Block Gas Ceiling for Binance Smart Chain
#### Tham khảo: [BEPs](https://github.com/binance-chain/BEPs)

