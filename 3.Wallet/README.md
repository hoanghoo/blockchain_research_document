﻿# Wallet & Testing ERC20 - BEP20

## Collection
[[Postman Collection]](https://bitbucket.org/vdc-team/researching-erc20-bep20/src/master/erc20_bep20/development/Wallet_API_web3/postman_collection/VDS.postman_collection.json)

## Install 
```
 npm install
```

## Run
```
 npm start
```
Service will be running at **localhost:3300**


Subsequent response definitions will only detail the expected value of the `data field`

## Routes
- Wallet
	- Network
		- List network
		- Create a network
		- Delete a network
	- Address
		- List address
		- Create an address
		- Delete an address
- Account
	- Native
		- Balance
- Contract
	- Contract
		- List contract
		- Deploy contract
- Transaction
	- ERC20_BEP20
		- Name
		- Symbol
		- Decimals
		- TotalSupply
		- BalanceOf
		- Transfer
		- Approve
		- Allowance
		- TransferFrom

### List Network

**Definition**
- Return network list
- `GET /wallet/network/list`
- 
**Expect Response**
- `200` Success
```json
{
	"3": {
		"name": "Ropsten",
		"RPC": "https://ropsten.infura.io/v3/888db53f946f4865b69bb17d98aa43ad"
	},
	"97": {
		"name": "Binance Smart Chain",
		"RPC": "https://data-seed-prebsc-1-s1.binance.org:8545"
	}
}
```

### Create Network

**Definition**
- Add a network
- `POST /wallet/network/create`

**Arguments**
```json
{
	"chainID":"5",
	"chainName":"name",
	"chainRPC":"abc"
}
```

**Expect Response**
- `201` Created

### Delete Network

**Definition**
- Delete a network
- `DELETE/wallet/network/delete`

**Arguments**
```json
{
	"chainID":"5"
}
```

**Expect Response**
- `200` Success


### List Address

**Definition**
- Return address list of chainID
- `GET /wallet/address/:chainID/list`

**Expect Response**
- `200` Success
```json
["0x9c3D57EaD49a1847ECFCDA774A7165927d7E5ABd"]
```

### Create Address

**Definition**
- Add an address of chainID
- `POST /wallet/address/:chainID/create`

**Arguments**

**Expect Response**
- `201` Created
```json
{
	"status": true,
	"chainID": "97",
	"address": "0xB4B1CdE172354B6b059777d1E5D2FDF8e745aE3d",
	"privateKey": "0x618cdb4de92eeb7dc36ffe289c1abb9b33a1eb0edd983a471361330bb35ecd63"
}
```

### Delete Address

**Definition**
- Delete an address
- `DELETE /wallet/address/:chainID/delete`

**Arguments**
```json
{
	"address":"0xB4B1CdE172354B6b059777d1E5D2FDF8e745aE3d"
}
```
**Expect Response**
- `200` Success

### Get Native Balance

**Definition**
- Get native balance (ETH/BNB)
- `GET /account/:chainID/balance`

**Arguments**
```json
{
	"address":"0x5a49D0444F0c2a73D82A7c4734c19C35e9BDCf27"
}
```
**Expect Response**
- `200` Success
```json
{
	"address": "0x5a49D0444F0c2a73D82A7c4734c19C35e9BDCf27",
	"balance": 0.991221622
}
```

### Get Contract List

**Definition**
- Get list contract of
- `GET /contract/:chainID/list`

**Expect Response**
- `200` Success
```json
["0x7a474Eaa017B2d03Af455B5Ba36450d5D200C5e5"]
```

### Deploy a token

**Definition**
- Deploy a token ERC20_BEP20
- `POST /account/:chainID/balance`

**Arguments**
```json
{
	"address":"0x877B6159AC48F638722C8F325DceA72a57947363",
	"name":"VIETNAM3",
	"symbol": "VND3",
	"decimals": 0,
	"totalSupply": 3000000
}
```
**Expect Response**
- `200` Success
```json
{
	"blockHash": "0x4e97486036bcd2e1c9dffa45b50f8311815db4bdeb1a3617d023d34e4be955d4",
	"blockNumber": 9606778,
	"contractAddress": "0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36",
	"cumulativeGasUsed": 2830155,
	"from": "0x877b6159ac48f638722c8f325dcea72a57947363",
	"gasUsed": 1216141,
	...
}
```

### Name

**Definition**
- Return token name
- `GET /transaction/:chainID/token/name`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "VIETNAM3"
}
```

### Symbol

**Definition**
- Return token symbol
- `GET /transaction/:chainID/token/symbol`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "VND3"
}
```


### Decimals

**Definition**
- Return token decimals
- `GET /transaction/:chainID/token/decimals`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "0"
}
```


### TotalSupply

**Definition**
- Return token total supply
- `GET /transaction/:chainID/token/totalSupply`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "3000000"
}
```

### BalanceOf

**Definition**
- Return balance of address
- `GET /transaction/:chainID/token/balanceOf`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36",
	"address":"0x877B6159AC48F638722C8F325DceA72a57947363"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "3000000"
}
```

### Transfer

**Definition**
- Transfer from _sender to _recipient _amount token
- `POST /transaction/:chainID/token/transfer`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36",
	"sender":"0x877B6159AC48F638722C8F325DceA72a57947363",
	"recipient":"0x76CAd360f1a9A6f28552dA39Bf453f352B4719c5",
	"amount":100
}
```
**Expect Response**
- `200` Success
```json
{
	"result": {
	"blockHash": "0x9271b4cc050331cdebb223fda949a3d2d6fbfd10dfe63489ea475eb89e8b0b12",
	"blockNumber": 9607011,
	"contractAddress": null,
	"cumulativeGasUsed": 477083,
	"from": "0x877b6159ac48f638722c8f325dcea72a57947363",
	"gasUsed": 51231,
	...
	"status": true,
	"to": "0x682eee0011ba9c7619ae6b1d8922321e4f924a36",
	"transactionHash": "0xe064a2b9b0b0887b619cb1afdafcb70e297512752818abcee2b19ec0fafe5135",
	}
}
```

### Approve

**Definition**
- Approve _amount token to _spender
- `POST /transaction/:chainID/token/approve`

**Arguments**
```json
{
	"contractAddress":"0xbb29b70f8B0Ba40715bd27b1443C0202f1845689",
	"sender":"0x5a49D0444F0c2a73D82A7c4734c19C35e9BDCf27",
	"spender":"0x3cd95381a66E4Db28B29704bCE59c9F475a1Aa89",
	"amount":500
}
```
**Expect Response**
- `200` Success

### Allowance

**Definition**
- Return allowance left what is approve from _owner to spender
- `GET /transaction/:chainID/token/allowance`

**Arguments**
```json
{
	"contractAddress":"0xbb29b70f8B0Ba40715bd27b1443C0202f1845689",
	"owner":"0x5a49D0444F0c2a73D82A7c4734c19C35e9BDCf27",
	"spender":"0x3cd95381a66E4Db28B29704bCE59c9F475a1Aa89"
}
```
**Expect Response**
- `200` Success
```json
{
	"result": "3000000"
}
```

### TransferFrom

**Definition**
- Transfer from _owner to _recipient _amount token (has been approved)
- `POST /transaction/:chainID/token/transferFrom`

**Arguments**
```json
{
	"contractAddress":"0x682eEe0011ba9C7619Ae6B1D8922321E4F924a36",
	"sender":"0x877B6159AC48F638722C8F325DceA72a57947363",
	"owner":"0x5a49D0444F0c2a73D82A7c4734c19C35e9BDCf27",
	"recipient":"0x76CAd360f1a9A6f28552dA39Bf453f352B4719c5",
	"amount":100
}
```
**Expect Response**
- `200` Success

