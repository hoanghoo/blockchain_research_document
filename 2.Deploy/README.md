﻿# Tài Liệu Kĩ Thuật - Triển khai Token ERC20_BEP20

#### Một số từ khóa quan trọng
- `ERC20`: Tiêu chuẩn phát triển token trên mạng Ethereum
- `BEP20`: Tiêu chuẩn phát triển token trên mạng Binance Smart Chain
- `EVM`: Ethereum Virtural Machine - máy ảo để chạy các hợp đồng thông minh trong mạng Ethereum và Binance Smart Chain
- `Solidity`: Ngôn ngữ viết smart contract.
- `Provider`: nhà cung cấp các  giao diện ngắn gọn, nhất quán cho các chức năng của một node mạng tiêu chuẩn
- `Web3`: Thư viện JS cung cấp các API tương tác với Smart Contract thông qua 1 Provider.
- `Metamask`: một wallet dạng extension trên Chrome.
- `Remix`: IDE hoàn chỉnh cung cấp bởi Ethereum cho phép biên soạn, complie, deploy smart contract trực tiếp qua web-view
- `ABI`: Application Binary Interface: interface của smart contract
- `Bytecode`: Đoạn mã được có thể chạy trên EVM dùng để triển khai hợp đồng thông minh

## 1. Tổng quan
#### 1.1 Định nghĩa
- Là quá trình triển khai smart contract lên mạng Blockchain thực. Những smart contract này định nghĩa về thông tin và các phương thức của token theo chuân ERC20 (đối với Ethereum) và BEP20 (đối với Binance Smart Chain).
#### 1.2 Các mạng Blockchain
- Có 2 loại public blockchain
	- **Mainnet:** ( còn được gọi Homestead) Đây là blockchain trên thực tế. Native token có giá trị thực trên mạng này. Triển khai này mang tính chất thử nghiệm và phát triển nên chúng ta không dùng mạng này.
	- **Testnet:**  Đây là test blockchain, bạn hãy tưởng tượng nó như một QA hoặc staging server. Nó được sử dụng chỉ cho mục đích test. Các token trong mạng này đều vô giá trị. Ở triển khai này chúng ta sử dụng mạng này
- Các mạng EVM và thông số kĩ thuật xem tại đây [[EVM Network]](https://chainid.network/)
- Trong triển khai này, ta sử dụng 2 mạng sau

| ChainID		| Name 											| Native Token    | Token Protocol | Explorer
| ------------- 	|----------											| --------------- 	|--------------		| ---
| 3      				| Ethereum Testnet Ropsten      	| ETH					|ERC20	| [Ropsten](https://ropsten.etherscan.io/)
| 97 				| Binance Smart Chain Testnet    	| BNB					|BEP20	| [BSC](https://testnet.bscscan.com/)

#### 1.3 Provider
- Provider là một abstract của kết nối với mạng Blockchain, cung cấp giao diện ngắn gọn, nhất quán cho các chức năng của một node mạng tiêu chuẩn. Nói cách khác, một client muốn giao tiếp với node mạng thì phải đi qua provider này
- Các kiểu Provider
	- Không cần chạy node mạng (EtherscanProvider, Infura): cung cấp khả năng kết nối với các nhà cung cấp bên thứ ba công khai mà không cần phải tự chạy bất kỳ nút Ethereum nào.
	- Phải chạy node mạng (JsonRpcProvider, IpcProvider): cho phép kết nối với các nút Ethereum mà bạn kiểm soát hoặc có quyền truy cập, bao gồm mainnet, testnet hoặc Ganache.
- MetaMask cung cấp sẵn Web3 provider (một loại JsonRpcProvider) liên kết với wallet.
- Trong triển khai này, ta sử dụng [Infura](https://infura.io/). Infura là nhà cung cấp cơ sở hạ tầng cho Ethereum. Infura hoạt động như một IaaS (Infrastructure as a Service). Lựa chọn này là vì những ưu điểm sau
	- Miễn phí trong môi trường dev và dự án nhỏ (<100.000 request/ngày)
	- Cộng đồng sử dụng rộng lớn, tuy nhiên đây cũng chính là nút cổ chai của mạng lưới phi tập trung khi hoạt động của Infura đang ảnh hưởng trực tiếp tới 90% tổng số node hiện tại của Ethereum. Tham khảo [[Infura - Nút cổ chai của mạng phi tập trung Ethereum]](https://sudungmaytinh.com/kiem-tien/blog-infura-nut-that-co-chai-tap-trung-cua-mang-luoi-phi-tap-trung-lon-nhat-the-gioi-ethereum/)
	- Nhanh chóng, ổn định, thiết lập đơn giản
#### 1.4 Sơ đồ tổng quát
![alt](https://ichi.pro/assets/images/max/724/1*ronMtzhop4EL70l8lItDGA.png)
#### 1.5 Điều kiện tiên quyết
- Đối với mạng Ropsten Ethereum Testnet
	- Có ví trên Ethereum (có thể tạo bằng MetaMask hoặc chính ứng dụng sau khi triển khai)
	- Số dư trong ví tối thiểu **0.002 ETH**, số dư này để thanh toán cho việc triển khai smart contract
	- Lấy ETH cho tài khoản tại : [[Ropsten Faucet]](https://faucet.ropsten.be/)
	
- Đối với mạng Binance Smart Chain Testnet
	- Có ví trên Ethereum (có thể tạo bằng MetaMask hoặc chính ứng dụng sau khi triển khai)
	- Số dư trong ví tối thiểu **0.002 BNB**, số dư này để thanh toán cho việc triển khai smart contract
	- Lấy ETH cho tài khoản tại : [[Binance Faucet]](https://testnet.binance.org/faucet-smart)

## 2. Các bước triển khai một token ERC20_BEP20
- Cách 1: Sử dụng Remix
	- Bước 1: Chuẩn bị : Smart Contract
	- Bước 2: Complie smart contract
	- Bước 3: Deploy smart contract với các tham số constructor (name, symbol, decimals, totalSupply)
- Cách 2: Sử dụng web3
	- Bước 1: Chuẩn bị: Smart Contract, Infura Provider
	- Bước 2: Complie smart contract, phần này tạo ra ABI và bytecode
	- Bước 3: Kết nối client với mạng Ethereum thông qua Provider
	- Bước 4: Tạo một giao dịch deploy smart contract với input là bytecode (sinh ra từ bước 2) và các tham số constructor (name, symbol, decimals, totalSupply)
	- Bước 5: Kí giao dịch này bằng private-key
	- Bước 6: Gửi giao dịch lên mạng

### Cách 1: Sử dụng Remix
#### Bước 1: Chuẩn bị
- Smart Contract: [VDS - Bitbucket](https://bitbucket.org/vdc-team/researching-erc20-bep20/src/master/erc20_bep20/development/SmartContract/)
- Remixd (Optional): Thư viện dùng để kết nối Remix với localhost, đơn giản hóa việc đưa smart contract và quản lý phiên bản smart contract bằng git
	`npm install -g @remix-project/remixd`
- Đưa smart contract vào Remix
	- Truy cập [Remix](https://remix.ethereum.org) 
	- Tạo các file và nội dung tương ứng trên Remix như mã nguồn smart contract
	- Nếu đã cài đặt Remixd, chạy script `remix-connect.sh`. Sau khi chạy script này truy cập Remix, trong **Workspace** chọn **-connect to localhost -** , chọn **connect**
- Kết nối Metamask với Remix (hướng dẫn trong bước 2)

#### Các bước còn lại
- Tham khảo [[Remix - Deploy Token]](https://docs.binance.org/smart-chain/developer/deploy/remix.html)

### Cách 2: Sử dụng Web3
#### Bước 1: Chuẩn bị
- Smart Contract: [VDS - Bitbucket](https://bitbucket.org/vdc-team/researching-erc20-bep20/src/master/erc20_bep20/development/SmartContract/)
- Remixd (Optional): Thư viện dùng để kết nối Remix với localhost, đơn giản hóa việc đưa smart contract và quản lý phiên bản smart contract bằng git
	`npm install -g @remix-project/remixd`
- Đưa smart contract vào Remix
	- Truy cập [Remix](https://remix.ethereum.org) 
	- Tạo các file và nội dung tương ứng trên Remix như mã nguồn smart contract
	- Nếu đã cài đặt Remixd, chạy script `remix-connect.sh`. Sau khi chạy script này truy cập Remix, trong **Workspace** chọn **-connect to localhost -** , chọn **connect**
	- Provider (đối với Ethereum)
		- Truy cập [Infura](https://infura.io/)
		- Đăng nhập
		- Chọn tab Ethereum -> Create new project. Nhập tên Project
		- Chọn endpoint là Ropsten, lưu lại 2 địa chỉ RPC. Ở đây là dùng địa chi HTTPS. 
		Ví dụ `https://ropsten.infura.io/v3/888db53f946f4865b69bb17d98aa43ad`
	- Đối với Binance Smart Chain: mạng BSC đã dựng sẵn Provider cho testnet tại `https://data-seed-prebsc-1-s1.binance.org:8545`

#### Bước 2: Complie
- Ở đây ta sẽ sử dụng  Remix để complie. Quá trình complie tương tự cách 1. Tham khảo [[Remix - Deploy Token]](https://docs.binance.org/smart-chain/developer/deploy/remix.html)
- Sau quá trình complie, ta lưu trữ ABI và Bytecode của ERC20.sol lại

#### Bước 3: Kết nối client với mạng thông qua Provider
- Sử dụng Web3 để kết nối với Provider qua HTTP-RPC
	`let web3 = new Web3("https://ropsten.infura.io/v3/888db53f946f4865b69bb17d98aa43ad")
	`
#### Bước 4: Tạo một giao dịch deploy smart contract 
- Tạo một smart contract mới với tất cả các method và event của nó được xác định trong ABI từ bước 2
`let  contract = new  web3.eth.Contract(ERC20_BEP20.ABI);`
- Định nghĩa contract là deploy contract với bytecode và các tham số constructor. Hàm này trả về một phiên bản contract mới.
	``let  contract_encode = contract.deploy({data :  bytecode, arguments: [tokenName, tokenSymbol, tokenDecimals, tokenTotalSuppy]}).encodeABI();``
#### Bước 5: Kí giao dịch này bằng private-key
- Quyết định gasLimit (ở dạng Hexadecimal)
`let  tx = {data : contract_encode, gas: '0x1b7740'}`
- Kí giao dịch bằng private-key
`let signed = web3.eth.accounts.signTransaction(tx, privateKey);`
#### Bước 6: Gửi giao dịch lên mạng
- Gửi giao dịch đã được kí bởi privateKey lên mạng
	`web3.eth.sendSignedTransaction(signed.rawTransaction)`

