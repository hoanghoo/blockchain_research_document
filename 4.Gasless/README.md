﻿# Tài Liệu Kĩ Thuật: Gasless (Meta-Transaction)

Bất cứ ai muốn giao dịch trên nền tảng của Ethereum đều cần ETH để trả phí (gas). Điều này là rào cản lớn với đa số user khi sử dụng Dapp do phải sở hữu ETH. Tài liệu này đề cập tới  khái niệm gasless (còn gọi là meta) transactions, giúp người dùng không nhất thiết cần ETH để giao dịch. Đồng thời giới thiệu concept gasless cơ bản và concept Gas Station Network được sinh ra để giải quyết vấn đề này.

## 1. Meta-transaction
Mọi giao dịch trên mạng Ethereum đều yêu cầu gas, và người gửi cần có đủ Ether để trả phí gas này. Điều đó nghĩa là user phải mua đồng ETH trước khi sử dụng bất cứ Dapp nào, không chỉ tốn thời gian mà còn là rào cản lớn với với đa số  người có ít kinh nghiệm với tiền kỹ thuật số.
Và meta-transactions, một ý tưởng đơn giản ra đời nhằm giải quyết vấn đề: Một tổ chức (được gọi là relayer) sẽ thanh toán phí giao dịch thay cho người dùng. Người dùng chỉ cần ký một thông điệp bao chứa thông tin về giao dịch. Sau đó, Relayer sẽ lo các bước còn lại: ký giao dịch (nếu hợp lệ) đi kèm với các thông tin đó, lan truyền lên mạng Ethereum và trả phí gas. Bằng cách này, người dùng không cần thiết phải sở hữu Ether.

## 2. Gasless Basic
Concept gồm 3 thành phần cơ bản

-   **Signer —**  address không có ether và là người khởi tạo giao dịch. Đây cũng là address kí giao dịch
-   **Sender—** address có ether, là người sẽ nhận giao dịch từ Signer và chuyển tiếp tới contract đích, thanh toán phí gas
-   **Relay/Proxy —** contract sẽ đọc dữ liệu đã ký, xác thực nó dựa trên chữ ký được cung cấp và chuyển tiếp giao dịch đến contract đích.
![alt](https://miro.medium.com/max/362/1*NMR9bzm5A9qKfLzZ8i_44g.png)

Các bước thực hiện
- Step 1: Signer khởi tạo meta-transaction, bao gồm
	- Mã hóa phương thức cần thực hiện đối với contract đích
	- Mã hóa tham số phương thức cần thực hiện đối với contract đích
	- Meta-transaction được tạo từ 2 yếu tố trên
- Step 2: Kí lên meta-transaction (để đảm bảo rằng meta-transaction không bị giả mạo)
	- Kí bằng private-key của signer lên meta-transaction được tạo từ bước 1
	- Tạo chữ kí
- Step 3: Sender chuyển tiếp yêu cầu của Signer
	- Sender chuyển tiếp yêu cầu của Signer bằng cách gọi phương thức `forward(toAddress, data, signature)` trên Relayer/Proxy với toAddress là địa chỉ của contract đích (tokenContract), data và signature là kết quả của step 2
	- Sender thanh toán phí gas cho giao dịch forward này
- Step 4: Relayer/Proxy xác thực giao dịch nhờ vào các tham số truyền lên, nếu đúng và được phép, nó tiến hành chuyển tiếp tới contract đích
- Step 5: Giao dịch hoàn tất

## 3. Gas Station Network
![alt](https://docs.opengsn.org/assets/img/paymaster_needs_gas.7ef47ccb.png)


GNS là mạng phi tập trung giúp cải thiện khả năng sử dụng DApp mà không phải đánh đối khả năng bảo mật. Với GNS, user có thể tương tác với mạng Ethereum mà không cần ETH cho phí transaction. Một số trường hợp sử dụng của GNS
- Thanh toán gas bằng một loại token
- Thanh toán gas bằng fiat
- Quyền riêng tư: Cho phép rút token không cần ETH được gửi đến các địa chỉ ẩn 

### 2.1. Kiến trúc
![alt](https://docs.opengsn.org/assets/img/gsn_flow_full_layered.737dd3e4.jpg)

- **Client**
	- Một địa chỉ hợp lệ nhưng không có ETH để trả gas
	- Kí và gửi meta-transaction tới Relay Server
	- Trước khi Relay Server thanh toán gas, nó xác minh rằng nó sẽ được hoàn lại bởi Paymaster contract
- **Relay Server: one for all, all for one**
	- Cách tốt nhất là mọi Dapp nên triển khai Relay Server của riêng mình để cung cấp dịch vụ miễn phí cho user của mình và dùng chính nó để tính phí giao dịch với các Dapp khác
	- Nếu Relay Server không khả dụng (bị tấn công), user sẽ dự phòng định tuyến tới các Relay Server của các Dapp khác
	- Điều này tạo ra hiệu ứng "One for all, all for one". Càng nhiều Dapp tham gia thì tính khả dụng của mạng càng mạnh mẽ
- **RelayHub : Kết nối người tham gia một cách đáng tin cậy**
	- RelayHub kết nối client, Relay Server và Paymaster trong khi những người này không cần biết hoặc tin tưởng lẫn nhau
	- Bên cạnh đó RelayHub giúp client tìm được Relay Server tốt nhất nếu Relay Server của mình gặp sử cố, cân bằng lại ETH của các Relay Server và đảm bảo rằng Paymaster trả lại cho các Relay Server phí gas cộng với phí giao dịch
- **Paymaster: Đồng ý hoàn lại phí gas cho Relay Server**
	- Trong GSN, tất cả logic kiểm soát truy cập và hoàn trả  gas được thực hiện bên trong Paymaster contract. Một paymaster có một bình ETH trong RelayHub và có thể thực hiện bất kỳ logic  nào để quyết định chấp nhận hay từ chối một giao dịch meta. Ví dụ: chỉ chấp nhận các giao dịch của người dùng trong whitelist, hoặc đối với các phương thức hợp đồng bắt buộc người dùng phải vượt qua capcha, v.v.
- **Trusted Forwarder: Xác thực chữ kí và nonce của sender**
	- Meta-transaction chỉ dựa vào một contract chuyển tiếp tin cậy nhỏ để bảo mật. Contract này xác minh chữ kí và nonce của người gửi ban đầu.
- **Recipient Contract: xem sender gốc**
	- Để hỗ trợ meta-transaction, phần này trả về sender ban đầu (sender kí giao dịch meta hoặc msg.sender nếu gọi trực tiếp)






